## Hello World 
Ini adalah dokumentasi dari pembuatan container berbasis nginx sebagai web server.
Didalam repo ini berisi beberapa file, antara lain Dockerfile dan index.html
Sebelum memulai ada beberapa prasarat:
```
1. Sudah menginstall docker engine di laptop
2. Memiliki akun di hub.docker.com
3. Sudah menginstall gcloud sdk di laptop
4. Memiliki service account yang akan digunakan sebagai otorisasi deploy ke GKE cluster
```
#### Dockerfile
Dockerfile berisi sekumpulan perintah/script yang akan dijalankan berurutan untuk membangun suatu container images. Direpo ini dan file index.html, atau perintah lain yang digunakan untuk menginstall dependencies,
Setelah memilih images dengan perintah **"FROM nginx:latest"**` kemudian menyalin file index.html yang beriisi echo "Hello World" ke **/usr/share/nginx/html/index.html** didalam container

### Docker build di local
Selanjutnya kita membangun images berdasarkan Dockerfile yang sudah dibuat sebelumnya dengan menggunakan perintahperintah, 
```
$docker build -t helloworld:latest .
```
Perintah diatas akan membangun sebuah container images dengan nama helloword dan tag/label latest
Tunggu sampai proses build selesai dengan munculnya 

### Menjalakan di local/laptop menggunakan docker engine
Setelah build Dockerfile berhasil selanjutnya kita akan menjalakan docker/container images yang sebelumnya sudah di build dengan perinta: 
```
$docker run -d -p 8080:80 --name helloworld helloworld
```
Maksud dari perintah diatas yaitu kita menjalakan container dengan images **helloworld** yang sudah di build sebelumnya, serta container tersebut akan diberi nama **hello-world**. Untuk perintah **-p** artinya container tersebut di expose ke host dengan port: **8080**, sedangkan didalam container berjalan di port **80**, perintah **-d** berarti ketika container tersebut running dalam mode detached (tidak terlihat output dari proses startup )
Contoh output bila kita berhasil menjalan sebuah container/docker images:
```
bd314ec56334b77a9c07dc8eab158a83fb2d26c1d152deb4b4b4d0c34966f80b
```
### Pengetesan / validasi service
Kita bisa menggunakan curl/browser untuk validasi/pengetesan dengan cara

1. Buka  `localhost:8080` di browser
2. **curl localhost:8080** di terminal
Berikut contoh hasil dari **curl localhost:8080** di terminal

<details><summary>Hasil curl</summary>
<!DOCTYPE html>
<html>

<head>
<title> Hi Folks, this is test. </title>
</head>

<body>
<h1>"Hello World"</h1>
<p><i>This is echo from container.</i></p>
</body>

</html>
</details>

### Push to docker repository
Supaya container images kita bisa digunakan oleh orang/tim lain ada beberapa cara diantaranya kita push ke public/private repository. Sebagai contoh disini kita akan melakukan push ke public repository yaitu *https://hub.docker.com/*. Berikut langkahnya:
```
$ docker login #selanjut muncul form masukan username & password 
$ docker tag helloworld hatab/nginx-helloworld:latest
$ docker push hatab/nginx-helloworld:latest
```
>Berikut contoh hasil upload/push ke docker repository : https://hub.docker.com/repository/docker/hatab/nginx-helloworld 

---
## Deploy ke GKE
### Login menggunakan services account
Setelah mendapatkan file service-account.json, kemudian login menggunakan file tersebut.
```
$gcloud auth activate-service-account services-account@project-id.iam.gserviceaccount.com --key-file=/path/location/service-account.json
$gcloud auth list #pengecekan akun yang aktif
$gcloud config list project #cek project id
```
### Mendapatkan credentials untuk cluster kuberneters
```
$gcloud container clusters get-credentials [CLUSTER-NAME]
```
### Deploy services/apps ke gke cluster
Deployment menggunakan kubectl dari file helloworld.yaml:
```
$kubectl apply -f helloworld.yaml
```
Untuk memastikan pods yang kita deploy sudah running:
```
$kubectl get pods
```
Untuk melihat logs
```
$ kubectl logs -f [NAMA-PODS]
```
Untuk melihat deployment helloworld
```
$kubectl get deployments helloworld
$kubectl describe deployments helloworld
```
### Expose services/apps ke public/internet, menggunakan loadbalancer GKE.
```
$kubectl apply -f  lb-helloworld.yaml
```
Cek IP Public
```
$kubectl get service lb-helloworld --output yaml |grep ip:
```
Catat EXTERNAL-IP, untuk pengecekan menggunakan curl atau browser:
```
$curl http://<external-ip>
```

#### Contoh hasil curl external-ip 
```
 $curl 34.135.36.24
```
<details><summary>Hasil curl</summary>
<!DOCTYPE html>
<html>

<head>
<title> Hi Folks, this is test. </title>
</head>

<body>
<h1>"Hello World"</h1>
<p><i>This is echo from container.</i></p>
</body>

</html>
</details>

Demikian penjelasan mengenai _docker_ _build_, _push_ dan _deploy_ ke **GKE**.

> [referensi](https://cloud.google.com/kubernetes-engine/docs/how-to/exposing-apps)
